var app = angular.module('app', []);

app.config(function($routeProvider) {
    $routeProvider.when('/',{
        templateUrl: 'app.html',
        controller: function($scope) {
            $scope.moveImp = function(id) {
                for (var i in $scope.data.issues)
                    if ($scope.data.issues[i].id == id)
                        $scope.data.issues[i].is_upcoming = !$scope.data.issues[i].is_upcoming;
            };
        }
    }).when('/issue/:issueid',{
        templateUrl: 'issue.html',
        controller: function($scope,$routeParams) {
            if ($routeParams.issueid)
                for (var i in $scope.data.issues)
                    if ($scope.data.issues[i].id == $routeParams.issueid)
                        $scope.issue = $scope.data.issues[i];
            
            $scope.addIssue = function() {
                $scope.issue.proposals.push({title: $scope.newIssue});
                $scope.newIssue = "";
            };
        }
    }).otherwise({
        redirectTo: '/'
    });
});

app.controller('sourceCtrl',function($scope,$http) {
    $http.get('js/data.json').success(function(text){
        $scope.data = text;
    });
});

